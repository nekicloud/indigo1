//
//  Slide.swift
//  indigotest
//
//  Created by Luka on 21/03/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import Charts
import WMGaugeView

class Slide: UIView {
    
    @IBOutlet weak var titleLbl: UILabel!
    
    var currentDataEntry = PieChartDataEntry(value: 0)
    @IBOutlet weak var chartView: PieChartView!
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var horizontalBarChartView: HorizontalBarChartView!
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    var colors = [UIColor]()
    var numberOfDataEntries = [PieChartDataEntry]()
    let myGauge = WMGaugeView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
   

    
    
    func setGaugeChart(toGauge:Double){
        myGauge.center = CGPoint(x: frame.width/2, y: frame.height/2)
        myGauge.backgroundColor = UIColor.clear
        let gaugeLabel = UILabel(frame: CGRect(x: 0, y: myGauge.frame.height, width: 100, height: 40))
        gaugeLabel.center = CGPoint(x: myGauge.frame.width/2, y: myGauge.frame.height)
        gaugeLabel.textColor = UIColor.white
        gaugeLabel.font = UIFont(name: "Helvetica Neue", size: 25)
        //gaugeLabel.textAlignment = .center
        gaugeLabel.text = String(toGauge) + "%"
        
        myGauge.needleStyle = WMGaugeViewNeedleStyleFlatThin
        myGauge.needleWidth = 0.03
        myGauge.needleScrewRadius = 0
        
        
        
        myGauge.minValue = 0
        myGauge.maxValue = 100
        myGauge.rangeValues = [10, 20, 100]        
        myGauge.rangeLabels = ["", "", ""]
        myGauge.rangeLabelsFontColor = UIColor.white
        myGauge.showRangeLabels = true
        myGauge.rangeLabelsWidth = 0.03
        
        myGauge.unitOfMeasurementFont = UIFont (name: "Helvetica Neue", size: 20)
        
        myGauge.showInnerBackground = false
        
        
        myGauge.scaleDivisions = 10; //broj velikih crtica
        myGauge.scaleSubdivisions = 1; // broj malih crtica, kad je 0 ne prikazuje uopste
        
        myGauge.scaleDivisionsWidth = 0.00;
        myGauge.scaleDivisionsLength = 0.02;
        
        myGauge.setValue( Float(toGauge) , animated: true, duration: 10)
        
        myGauge.addSubview(gaugeLabel)
        self.addSubview(myGauge)
        
    }
    
    func setHorizontalChart(buyers2: [Buyers]) {
        horizontalBarChartView.isHidden = false
        
        let colors1 = [UIColor.green,UIColor.orange,UIColor.red]
        var dataEntries: [ChartDataEntry] = []
        var companies: [String] = []
        var i = 1
        for buyers in buyers2{
            let dataEntry = BarChartDataEntry(x:Double(i), yValues:[buyers.dugUValuti,buyers.dugVanValute,buyers.ukupno])
            
            i=i+1
            companies.append(buyers.naziv)
            dataEntries.append(dataEntry)
        }
        
        horizontalBarChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: companies)
        horizontalBarChartView.fitBars = true
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
        chartDataSet.colors = colors1
        chartDataSet.valueTextColor=UIColor.white
        horizontalBarChartView.xAxis.labelTextColor=UIColor.white
        let chartData = BarChartData(dataSet: chartDataSet)
   
        horizontalBarChartView.data = chartData
        horizontalBarChartView.xAxis.drawAxisLineEnabled = false
        horizontalBarChartView.xAxis.drawGridLinesEnabled = false
        horizontalBarChartView.leftAxis.enabled = false
        horizontalBarChartView.rightAxis.enabled = false
        horizontalBarChartView.drawBordersEnabled = false
        horizontalBarChartView.drawBordersEnabled = false
        horizontalBarChartView.minOffset = 0
        
    }
    
    func setBarChartExp(expend: [Expenditure]) {
        barChartView.isHidden = false
        
        let colors1 = [UIColor.green,UIColor.orange,UIColor.red]

        var dataEntries: [BarChartDataEntry] = []
        
        
        for i in 0..<expend.count {
            
            let dataEntry = BarChartDataEntry(x: Double(expend[i].proslaGodina), y: Double(expend[i].tekucaGodina))
            
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Visitor count")
        chartDataSet.colors = colors1
        chartDataSet.valueTextColor=UIColor.white
        barChartView.xAxis.labelTextColor=UIColor.white

        let chartData = BarChartData(dataSet: chartDataSet)
        
        barChartView.fitBars = true

        barChartView.data = chartData
        barChartView.xAxis.drawAxisLineEnabled = false
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.leftAxis.enabled = false
        barChartView.rightAxis.enabled = false
        barChartView.drawBordersEnabled = false
        barChartView.drawBordersEnabled = false
        barChartView.minOffset = 0
        
    }
    
    func setPieChart(dataEntry:[Invoice]) {
        
        chartView.isHidden=false
        var sum:Double = 0
        for Invoice in dataEntry{
            let testDataEntry = PieChartDataEntry(value:0)
            sum = sum + Invoice.sum!
            testDataEntry.value = Invoice.sum!
            testDataEntry.label = Invoice.name
            colors.append(hexStringToUIColor(hex: Invoice.color!))
            numberOfDataEntries.append(testDataEntry)
        }
        chartView.centerText = String(sum) + "$"
        updateChartData()
    }
    
    func updateChartData(){
        let chartDataSet = PieChartDataSet(values: numberOfDataEntries, label: nil)
        let chartData = PieChartData(dataSet: chartDataSet)
        
        chartDataSet.colors = colors
        
        chartView.data = chartData
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

}
