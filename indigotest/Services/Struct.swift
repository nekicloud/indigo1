//
//  Struct.swift
//  indigotest
//
//  Created by MacBook Pro on 21/03/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

struct Revenue: Codable{
    let mjesec: String?
    let proslaGodina: Double
    let tekucaGodina, target: Int
    
    enum CodingKeys: String, CodingKey {
        case mjesec
        case proslaGodina = "prosla_godina"
        case tekucaGodina = "tekuca_godina"
        case target
    }
}

struct Profit: Codable{
    let vrijednostIzlaznihFaktura, vrijednostUlaznihFaktura: Double
    let vrijednostTroskova: Int
    let profitMargin: Double
    
    enum CodingKeys: String, CodingKey {
        case vrijednostIzlaznihFaktura = "vrijednost_izlaznih_faktura"
        case vrijednostUlaznihFaktura = "vrijednost_ulaznih_faktura"
        case vrijednostTroskova = "vrijednost_troskova"
        case profitMargin = "profit_margin"
    }
}

struct Expenditure: Codable{
    let proslaGodina, tekucaGodina: Double
    let target: Int
    
    enum CodingKeys: String, CodingKey {
        case proslaGodina = "prosla_godina"
        case tekucaGodina = "tekuca_godina"
        case target
    }
}

struct Buyers: Codable{
    let id, naziv: String
    let ukupno, dugUValuti, dugVanValute: Double
    
    enum CodingKeys: String, CodingKey {
        case id, naziv, ukupno
        case dugUValuti = "dug_u_valuti"
        case dugVanValute = "dug_van_valute"
    }
}

struct Debt: Codable{
    let fakturisanoVanValute, dugVanValute, badDebt: Double
    
    enum CodingKeys: String, CodingKey {
        case fakturisanoVanValute = "fakturisano_van_valute"
        case dugVanValute = "dug_van_valute"
        case badDebt = "bad_debt"
    }
}

struct Invoice: Codable {
    let name: String?
    let sum: Double?
    let color: String?
    let idLinijeBiznisa: Int?
    
    enum CodingKeys: String, CodingKey {
        case name, sum, color
        case idLinijeBiznisa = "id_linije_biznisa"
    }
}

struct Supplier: Codable {
    let id, naziv: String?
    let ukupno, dugUValuti, dugVanValute: Double?
    
    enum CodingKeys: String, CodingKey {
        case id, naziv, ukupno
        case dugUValuti = "dug_u_valuti"
        case dugVanValute = "dug_van_valute"
    }
}

struct IndigoResponse<T: Codable>: Codable {
    let errorCode, itemsCount: Int?
    let data: [T]?
    
    enum CodingKeys: String, CodingKey {
        case errorCode = "error_code"
        case itemsCount = "items_count"
        case data
    }
}
