//
//  DashboardVC.swift
//  indigotest
//
//  Created by Luka on 21/03/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import Moya

class DashboardVC: UIViewController{
    
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var pageView: UIView!
 

    override func viewDidLoad() {
        super.viewDidLoad()
        setXib()

        menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        menuBtn.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        menuBtn.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())


    }


    func setXib(){
        let slide12:SlideView = Bundle.main.loadNibNamed("SlideView", owner: self, options: nil)?.first as! SlideView
        pageView.addSubview(slide12)
        pageView.bringSubviewToFront(slide12)
    }
    
    
  


}
