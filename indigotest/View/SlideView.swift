//
//  SlideView.swift
//  indigotest
//
//  Created by Luka on 04/04/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import Moya

class SlideView: UIView, UIScrollViewDelegate {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    var slides:[Slide] = []
    var activityIndicator = UIActivityIndicatorView()
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.activityIndicator = UIActivityIndicatorView(style: .white)
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.center = self.center
        addSubview(self.activityIndicator)
        activityIndicator.startAnimating()
        setSlides()
        scrollView.delegate = self
       
        pageControl.transform = CGAffineTransform(scaleX: 2, y: 2)
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        bringSubviewToFront(pageControl)
        
    }
    
    
    
    func setupSlideScrollView(slides : [Slide]) {
 
        scrollView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        scrollView.contentSize = CGSize(width: frame.width * CGFloat(slides.count), height: frame.height)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: frame.width * CGFloat(i), y: 0, width: frame.width, height: frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    
    
    func setSlides(){
        slides = createSlides()
        setupSlideScrollView(slides: slides)
    }
    
    
    func createSlides() -> [Slide] {
        
        let slide1:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide1.titleLbl.text = "Odnos loseg duga"
        group.enter()
        debtCall()
        group.notify(queue: .main) {
            self.activityIndicator.stopAnimating()
            slide1.setGaugeChart(toGauge:globalDebt)
        }
    
       
        
        let slide2:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide2.titleLbl.text = "Top 10 kupaca"
        group.enter()
        buyersCall()
        group.notify(queue: .main) {
     
             slide2.setHorizontalChart(buyers2: buyers)
            
        }
       
        
        let slide3:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide3.titleLbl.text = "Top 10 dobavljaca"
        group.enter()
        suppCall()
        group.notify(queue: .main) {
            
            slide3.setHorizontalChart(buyers2: suppliers)
            
        }
        
        let slide4:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide4.titleLbl.text = "Targeti prihodi(jbg ne radi call)"
//        group.enter()
//        revCall()
//        group.notify(queue: .main) {
//            slide4.setBarChartExp()
//
//        }

        
        let slide5:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide5.titleLbl.text = "Targeti rashodi"
        group.enter()
        expCall()
        group.notify(queue: .main) {
            
            slide5.setBarChartExp(expend: expenditure)

        }
        
        let slide6:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide6.titleLbl.text = "Profitabilnost"
        group.enter()
        profitCall()

        group.notify(queue: .main){
          slide6.setGaugeChart(toGauge: profits)
        }
        let slide7:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide7.titleLbl.text = "Vrijednost izlaznih faktura po linijima biznisa"
        group.enter()
        invoiceCall()

        group.notify(queue: .main){
            slide7.setPieChart(dataEntry: faktura1)
        }
        
        return [slide1, slide2, slide3, slide4, slide5, slide6, slide7]
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
    

}
