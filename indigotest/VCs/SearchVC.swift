//
//  SearchVC.swift
//  indigotest
//
//  Created by Luka on 07/04/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import SearchTextField
import DropDown

class SearchVC: UIViewController {

    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var firmName: SearchTextField!
    @IBOutlet weak var typeDrop: UIView!
    @IBOutlet weak var lineDrop: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        menuBtn.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        menuBtn.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        firmName.filterStrings(["test1", "test2" , "test3"])
        
        let dropMenu = DropDown()
        
        dropMenu.anchorView = typeDrop
        dropMenu.dataSource = ["test1", "test2" , "test3"]
//        dropMenu.selectionAction = { [unowned self] (index: Int, item: String) in
//            //
//        }
        dropMenu.show()

    
        
        

    }
    
    

}
