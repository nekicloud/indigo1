//
//  SearchView.swift
//  indigotest
//
//  Created by Luka on 04/04/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import SearchTextField

class SearchView: UIView {

    @IBOutlet weak var firmName: SearchTextField!
    
    override func awakeFromNib() {
        firmName.filterStrings(["test1", "test2" , "test3"])

    }
    

}
