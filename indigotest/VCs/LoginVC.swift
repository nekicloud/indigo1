//
//  ViewController.swift
//  indigotest
//
//  Created by Luka on 21/03/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func loginPressed(_ sender: Any) {
        performSegue(withIdentifier: "loginBtn", sender: nil)
    }
    
    @IBAction func checkBoxTapped(_ sender: UIButton){
        
       UIView.animate(withDuration: 0.05, delay: 0, options: .curveLinear, animations: {
        
        sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        sender.isSelected = !sender.isSelected
       }) { (success) in
        
        UIView.animate(withDuration: 0.05, delay: 0, options: .curveLinear, animations: {
            sender.transform = .identity
            }, completion: nil)
        
       }
        
    }


}

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}
