//
//  HorizontalChart.swift
//  indigotest
//
//  Created by Luka on 01/04/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import Charts

class HorizontalChart: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var titleLbl: UILabel!
    
    var currentDataEntry = PieChartDataEntry(value: 0)
    @IBOutlet weak var chartView: PieChartView!
    var colors = [UIColor]()
    var numberOfDataEntries = [PieChartDataEntry]()
    
    func setChart(dataEntry:[Invoice]) {
        
        print(dataEntry)
        print(dataEntry.first?.name)
        for fakturaa in dataEntry{
            print(fakturaa.name)
            print(fakturaa.sum)
            let testDataEntry = PieChartDataEntry(value:0)
            testDataEntry.value = fakturaa.sum!
            testDataEntry.label = fakturaa.name
            colors.append(hexStringToUIColor(hex: fakturaa.color!))
            numberOfDataEntries.append(testDataEntry)
            print(numberOfDataEntries)
        }
        updateChartData()
    }
    
    func updateChartData(){
        let chartDataSet = PieChartDataSet(values: numberOfDataEntries, label: nil)
        let chartData = PieChartData(dataSet: chartDataSet)
        
        chartDataSet.colors = colors
        
        chartView.data = chartData
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }


}
