//
//  JsonCalls.swift
//  indigotest
//
//  Created by Luka on 04/04/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation
import Moya
let provider = MoyaProvider<IndigoApp>()
let group = DispatchGroup()
var globalDebt:Double = 0
var profits:Double = 0
var faktura1 = [Invoice]()
var buyers = [Buyers]()
var suppliers = [Buyers]()
var revenue = [Revenue]()
var expenditure = [Expenditure]()


func debtCall(){

    provider.request(.debt) { result in
        switch result {
        case let .success(moyaResponse):
            let data = moyaResponse.data
            let statusCode = moyaResponse.statusCode
            let decoded = try? JSONDecoder().decode(IndigoResponse<Debt>.self, from: data)
            if let response : IndigoResponse = decoded{
                DispatchQueue.main.async{
                    globalDebt = (response.data!.first?.badDebt)!
                    group.leave()
                  
                }
                
            }
            
        case let .failure(error):
            // TODO: handle the error == best. comment. ever.
            print(error)
        }
        

}
    
        
        

    
}

func profitCall(){
    
    provider.request(.profitability(1)) { result in
        switch result {
        case let .success(moyaResponse):
            let data = moyaResponse.data
            let statusCode = moyaResponse.statusCode
            let decoded = try? JSONDecoder().decode(IndigoResponse<Profit>.self, from: data)
            if let response : IndigoResponse = decoded{
                DispatchQueue.main.async {
                    profits = (response.data!.first?.profitMargin)!
                    group.leave()
                    
                }
            }
            
        case let .failure(error):
            // TODO: handle the error == best. comment. ever.
            print(error)
        }
        
        
    }
}

func invoiceCall(){
    provider.request(.invoice) { result in
                switch result {
                case let .success(moyaResponse):
                    let data = moyaResponse.data
                    let statusCode = moyaResponse.statusCode
                    let decoded = try? JSONDecoder().decode(IndigoResponse<Invoice>.self, from: data)
        
                    if let response : IndigoResponse = decoded{
                        DispatchQueue.main.async {
                            faktura1=response.data!
                            group.leave()
                        }
                    }
        
                // do something in your app
                case let .failure(error):
                    // TODO: handle the error == best. comment. ever.
                    print(error)
                }
        
        }
    
}

func buyersCall(){
    provider.request(.buyers) { result in
        switch result {
        case let .success(moyaResponse):
            let data = moyaResponse.data
            let statusCode = moyaResponse.statusCode
            let decoded = try? JSONDecoder().decode(IndigoResponse<Buyers>.self, from: data)
            
            if let response : IndigoResponse = decoded{
                DispatchQueue.main.async {
                    buyers=response.data!
                    group.leave()
                }
            }
            
        // do something in your app
        case let .failure(error):
            // TODO: handle the error == best. comment. ever.
            print(error)
        }
        
    }
}

func suppCall(){
    provider.request(.suppliers) { result in
        switch result {
        case let .success(moyaResponse):
            let data = moyaResponse.data
            let statusCode = moyaResponse.statusCode
            let decoded = try? JSONDecoder().decode(IndigoResponse<Buyers>.self, from: data)
            
            if let response : IndigoResponse = decoded{
                DispatchQueue.main.async {
                    suppliers=response.data!
                    group.leave()
                }
            }
            
        // do something in your app
        case let .failure(error):
            // TODO: handle the error == best. comment. ever.
            print(error)
        }
        
    }
}

func revCall(){
    provider.request(.revenue(1)) { result in
        switch result {
        case let .success(moyaResponse):
            let data = moyaResponse.data
            let statusCode = moyaResponse.statusCode
            let decoded = try? JSONDecoder().decode(IndigoResponse<Revenue>.self, from: data)
            if let response : IndigoResponse = decoded{
                DispatchQueue.main.async {
                    
                    revenue=response.data!
                    group.leave()
                }
            }else{
                print("ne valja auth key")
                group.leave()
            }
            
            
        case let .failure(error):
            // TODO: handle the error == best. comment. ever.
            print(error)
        }
        
    }
}

func expCall(){
    provider.request(.expenditure(1)) { result in
                switch result {
                case let .success(moyaResponse):
                    let data = moyaResponse.data
                    let statusCode = moyaResponse.statusCode
                    let decoded = try? JSONDecoder().decode(IndigoResponse<Expenditure>.self, from: data)
                    if let response : IndigoResponse = decoded{
                        DispatchQueue.main.async {
                            expenditure=response.data!
                            group.leave()
                        }
                    }
        
                case let .failure(error):
                    // TODO: handle the error == best. comment. ever.
                    print(error)
                }
        
            }
}
