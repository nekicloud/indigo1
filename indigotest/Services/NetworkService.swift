//
//  File.swift
//  indigotest
//
//  Created by MacBook Pro on 21/03/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

import Moya

enum IndigoApp{
    case debt
    case buyers
    case suppliers
    case revenue(Int)
    case expenditure(Int)
    case profitability(Int)
    case invoice
    
    
}

extension IndigoApp: TargetType {
    
    var headers: [String: String]? {
        return ["Content-type": "application/json", "Authorization":"API_KEY819fd567ccce2695e30e4ae096672f7b"]
    }
    
    var baseURL: URL { return URL(string: "https://indigoerp.me/")! }
    
    var path: String {
        switch self {
        case .debt:
            return "indigo/web_servisi_ios_obuka/_odnosLosegDuga.php"
        case .buyers:
            return "indigo/web_servisi_ios_obuka/_top10kupaca.php"
        case .revenue:
            return "indigo/web_servisi_ios_obuka/_targetiPrihodi.php"
        case .expenditure:
            return "indigo/web_servisi_ios_obuka/_targetiRashodi.php"
        case .profitability:
            return "indigo/web_servisi_ios_obuka/_profitMargin.php"
        case .invoice:
            return "indigo/web_servisi_ios_obuka/_firstGraph.php"
        case .suppliers:
            return "/indigo/web_servisi_ios_obuka/_top10dobavljaca.php"
            
        }
    }
    var method: Moya.Method {
        switch self {
        case .debt:
            return .get
        case .buyers:
            return .get
        case .revenue:
            return .get
        case .expenditure:
            return .get
        case .profitability:
            return .get
        case .invoice:
            return .get
        case .suppliers:
            return .get
        }
        
    }
    
    var task: Task {
        switch self {
        case .debt:
            return .requestPlain
        case .buyers:
            return .requestPlain
        case .revenue(let month):
            var params: [String: Any] = [:]
            params["vrsta"] = 1
            params["mjesec"] = month
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .profitability(let month):
            var params: [String: Any] = [:]
            params["vrsta"] = 1
            params["mjesec"] = month
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .expenditure(let month):
            var params: [String: Any] = [:]
            params["vrsta"] = 1
            params["mjesec"] = month
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .invoice:
            var params: [String: Any] = [:]
            params["godina"] = 1
            params["tip"] = 2
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .suppliers:
            return .requestPlain
        }
    }
    
    var sampleData: Data {
        return Data()
        
    }
}


