//
//  MenuCell.swift
//  indigotest
//
//  Created by Luka on 27/03/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var menuName: UILabel!
    @IBOutlet weak var menuImage: UIImageView!

}
