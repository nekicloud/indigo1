//
//  MenuVC.swift
//  indigotest
//
//  Created by Luka on 21/03/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import Charts

class MenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    var dashboardVC: DashboardVC!
    var searchVC: SearchVC!
    
    let menuItems = [
        Menu(image: "group63", name: "Dashboard"),
        Menu(image: "group47", name: "Pretraga Faktura"),
        Menu(image: "group56", name: "Pretraga Ugovora"),
        Menu(image: "group57", name: "Pretraga Firmi"),
        Menu(image: "group59blck", name: "Alarmi"),
        Menu(image: "settings5", name: "Podešavanja")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.revealViewController()?.rearViewRevealWidth = self.view.frame.size.width - 45
        
        dashboardVC = self.revealViewController()?.frontViewController as? DashboardVC
        searchVC = UIStoryboard(name: "Menuboard", bundle: nil).instantiateInitialViewController() as? SearchVC
        
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as? MenuCell{
            let menu = menuItems[indexPath.row]
            cell.menuImage.image = UIImage(named: menu.image)
            cell.menuName.text = menu.name
            return cell
        }
        else{
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 60.0;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.revealViewController()?.pushFrontViewController(dashboardVC, animated: true)
        case 1:
            self.revealViewController()?.pushFrontViewController(searchVC, animated: true)
        default:
            break
        }
        
    }

}

struct Menu: Codable{
    let image: String
    let name: String
    
    public init(image: String, name: String) {
        self.image = image
        self.name = name
    }
    
}
